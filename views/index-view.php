<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ozlabs</title>
    <link rel="icon" href="img/favicon.ico" type="image/jpg">
    <link rel="stylesheet" href="css/app.css" />
    <script src="js/app.js" defer></script>
    <script src="js/svg-img.js"></script>
    <?php require_once __DIR__ . "/../src/live-reload.php"; ?>
</head>

<body>
    <header>
        <button id="toggle-theme-button" class="circle">
            <svg-img id="toggle-theme-button-image" src="img/light-mode.svg"></svg-img>
        </button>
    </header>
    <main>
        <h1>Ozlabs</h1>
        <p>comming soon</p>
    </main>
</body>

</html>