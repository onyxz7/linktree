"use strict";

let toggleThemeButton = document.getElementById("toggle-theme-button");
let toggleThemeButtonImage = document.getElementById("toggle-theme-button-image");

// Initialize theme.
const theme = getTheme();
document.body.dataset.theme = theme;
toggleThemeButtonImage.src = `img/${theme}-mode.svg`;

// Toggle theme button.
toggleThemeButton.addEventListener("click", () => {
    let theme = getTheme();
    theme = theme === "light" ? "dark" : "light";
    setTheme(theme);

    document.body.dataset.theme = theme;
    toggleThemeButtonImage.src = `img/${theme}-mode.svg`;
});

// Theme functions.
function getTheme() {
    return localStorage.getItem("theme") ?? "light";
}

function setTheme(theme) {
    localStorage.setItem("theme", theme);
}