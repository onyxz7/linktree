class SvgImg extends HTMLElement {
    static observedAttributes = ["src", "alt"];

    get src() { return this.getAttribute("src") }
    set src(value) { return this.setAttribute("src", value) }

    get alt() { return this.getAttribute("alt") }
    set alt(value) { return this.setAttribute("alt", value) }

    constructor() {
        super();

        this.shadow = this.attachShadow({ mode : "open"});
    }

    attributeChangedCallback(name, _, newValue) {
        if (name === "src") this.updateImg(newValue);
    }

    async updateImg(src) {
        try {
            let response = await fetch(src);
            if (response.ok) this.shadow.innerHTML = await response.text();
        } catch (e) {
            this.shadow.innerHTML = "";
        }
    }
}
customElements.define("svg-img", SvgImg);